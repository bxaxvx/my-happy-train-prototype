﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StartingTrainInfo
{
    public TrainColor color;
    public Rail[] startingRails;
    public float speedModifier;
    public Direction startingDirection;
    public GameObject trainPrefab;
    public GameObject carriagePrefab;
}

[System.Serializable]
public class Quest
{
    public TrainColor color;
    public Station spawnPoint;
    public Station destinationPoint;
}

public class Level : MonoBehaviour
{
    public int cameraSize;
    public AudioClip music;
    public UISlide introSlide;
    public UISlide nextSlide;
    public StartingTrainInfo[] trainLocations;
    public Station[] stations;
    public int maxQuestNumber;
    public int victoryMoney;

    private List<Quest> quests = new List<Quest>();

    private void SpawnQuests()
    {
        int questNumber = quests.Count;
        for (int i = questNumber; i < maxQuestNumber; i++)
        {
            GenerateQuest();
        }
    }

    private void GenerateQuest()
    {
        List<TrainColor> availableColors = new List<TrainColor>();// { TrainColor.Blue, TrainColor.Green, TrainColor.Red, TrainColor.Yellow };
        foreach (var train in trainLocations)
            availableColors.Add(train.color);
        List<Station> spawnStations = new List<Station>(stations);
        List<Station> targetStations = new List<Station>(stations);
        foreach (var quest in quests)
        {
            availableColors.Remove(quest.color);
            spawnStations.Remove(quest.spawnPoint);
        }
        Quest newQuest = new Quest();
        if (availableColors.Count == 0 || spawnStations.Count == 0)
            return;
        newQuest.color = availableColors[Random.Range(0, availableColors.Count)];
        newQuest.spawnPoint = spawnStations[Random.Range(0, spawnStations.Count)];
        targetStations.Remove(newQuest.spawnPoint);
        if (targetStations.Count == 0)
            return;
        newQuest.destinationPoint = targetStations[Random.Range(0, targetStations.Count)];
        newQuest.spawnPoint.SpawnQuestBox(newQuest);
        quests.Add(newQuest);
    }

    private List<GameObject> trains = new List<GameObject>();

    public void ResetLevel()
    {
        for (int i = trains.Count - 1; i >= 0; i--)
            Destroy(trains[i]);
        foreach (var station in stations)
            station.Reset();
        trains.Clear();
        quests.Clear();
    }

    public void StartGame()
    {
        GameManager.Instance.SetCameraSize(cameraSize);
        if (music != null)
            GameManager.Instance.PlayMusic(music);
        ResetLevel();
        int trainNumber = 0;
        GameManager.Instance.DelayFunction(SpawnQuests, 5f);
        foreach (var train in trainLocations)
        {
            trainNumber++;
            var trainGO = new GameObject("Train " + trainNumber);
            List<Carriage> carriages = new List<Carriage>();
            for (int i = 1; i < train.startingRails.Length; i++)
            {
                Carriage carriage = Instantiate(train.carriagePrefab, trainGO.transform).GetComponent<Carriage>();
                carriage.Init(train.startingRails[i], train.startingDirection, train.speedModifier, train.color);
                carriages.Add(carriage);
            }
            Instantiate(train.trainPrefab, trainGO.transform).GetComponent<Train>().Init(train.startingRails[0], train.startingDirection, train.speedModifier, train.color, carriages);
            trains.Add(trainGO);
        }
        gameObject.SetActive(true);
    }

    public void RemoveQuest(Quest quest)
    {
        quests.Remove(quest);
        GameManager.Instance.DelayFunction(SpawnQuests, 6f);
    }
}
