﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerRail : Rail
{
    public override Direction GetDirection(Direction fromDirection)
    {
        return GameManager.InvertedDirection(fromDirection) == direction1 ? direction2 : direction1;
    }

    [SerializeField]
    private Direction direction1;
    [SerializeField]
    private Direction direction2;
}
