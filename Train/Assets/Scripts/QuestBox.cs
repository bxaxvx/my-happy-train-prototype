﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestBox : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private Sprite[] boxSprites;
    public Quest quest;

    public void Init(Quest quest)
    {
        spriteRenderer.sprite = boxSprites[(int)quest.color];
        this.quest = quest;
    }

}
