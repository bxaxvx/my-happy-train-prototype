﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<UIManager>();
            return instance;

        }
    }
    [SerializeField]
    private UISlide startingSlide;
    [SerializeField]
    private Image[] hearts;
    [SerializeField]
    private Text moneyText;

    private void Awake()
    {
        startingSlide.Show();
    }
    public void SetHealth(int number)
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            hearts[i].enabled = i < number;
        }
    }

    public void SetMoney(int number, int maxNumber)
    {
        moneyText.text = "$" + number + "/" + maxNumber;
    }
}
