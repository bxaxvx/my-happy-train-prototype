﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Rail : MonoBehaviour
{
    [SerializeField]
    protected SpriteRenderer spriteRenderer;
    public abstract Direction GetDirection(Direction fromDirection);
}
