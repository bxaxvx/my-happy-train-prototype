﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Station : MonoBehaviour
{
    [SerializeField]
    private Image[] questRenderers;
    private List<TrainColor> displayedQuestColors = new List<TrainColor>();
    private QuestBox box;

    public void SpawnQuestBox(Quest newQuest)
    {
        box = Instantiate(GameManager.Instance.boxPrefab, transform).GetComponent<QuestBox>();
        box.Init(newQuest);
    }

    public void AddQuest(TrainColor color)
    {
        displayedQuestColors.Add(color);
        UpdateQuests();
    }

    public void RemoveQuest(TrainColor color)
    {
        displayedQuestColors.Remove(color);
        UpdateQuests();
    }

    private void UpdateQuests()
    {
        foreach (var questRenderer in questRenderers)
            questRenderer.enabled = false;
        int index = 0;
        foreach (var color in displayedQuestColors)
        {
            questRenderers[index].sprite = GameManager.Instance.GetCarriageSprite(color);
            questRenderers[index].enabled = true;
            index++;
        }
    }

    internal void Reset()
    {
        displayedQuestColors.Clear();
        UpdateQuests();
        if (box != null)
            Destroy(box.gameObject);
    }
}
