﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISlide : MonoBehaviour
{
    public UISlide nextSlide;
    public bool lastSlide;
    public bool loadLevel;
    public int levelNumber;

    public void Proceed()
    {
        Hide();
        if (nextSlide != null)
            nextSlide.Show();
        else if (lastSlide)
            Application.Quit();
        else if (loadLevel)
            GameManager.Instance.StartLevel(levelNumber);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        if (lastSlide)
            GameManager.Instance.PlayVictoryMusic();
    }

}
