﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<GameManager>();
            return instance;

        }
    }

    private bool isGameOver;
    public bool IsGameOver
    {
        get
        {
            return isGameOver;
        }
    }

    public static float GlobalSpeedMultiplier = 1f;

    [SerializeField]
    private Level[] levels;
    [SerializeField]
    private Sprite[] carriageSprites;
    [SerializeField]
    private AudioClip victoryMusic;

    public void PlayVictoryMusic()
    {
        PlayMusic(victoryMusic);
    }

    [SerializeField]
    private AudioClip[] sounds;
    [SerializeField]
    private AudioSource sfxSource;
    [SerializeField]
    private AudioSource musicSource;
    [SerializeField]
    private Camera camera;

    public void PlaySound(SoundType sound)
    {
        sfxSource.PlayOneShot(sounds[(int)sound]);
    }

    public void PlayMusic(AudioClip music)
    {
        musicSource.clip = music;
        musicSource.time = 0;
        musicSource.Play();
    }

    private int health;
    private int money;
    public GameObject boxPrefab;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public void SetCameraSize(int size)
    {
        camera.orthographicSize = size;
    }

    public Sprite GetCarriageSprite(TrainColor color)
    {
        return carriageSprites[(int)color];
    }

    public void AddMoney(int delta)
    {
        money += delta;
        UIManager.Instance.SetMoney(money, currentLevel.victoryMoney);
        if (money >= currentLevel.victoryMoney)
            Victory();
    }

    private void Victory()
    {
        health = 4;
        DelayFunction(currentLevel.ResetLevel, 3f);
        DelayFunction(() => currentLevel.gameObject.SetActive(false), 3.1f);
        DelayFunction(currentLevel.nextSlide.Show, 3f);
    }

    private Level currentLevel;

    public void StartLevel(int levelNumber)
    {
        isGameOver = false;
        health = 4;
        money = 0;
        currentLevel = levels[levelNumber];
        UIManager.Instance.SetHealth(health);
        UIManager.Instance.SetMoney(money, currentLevel.victoryMoney);
        currentLevel.StartGame();
    }

    public static Direction InvertedDirection(Direction direction)
    {
        if (direction == Direction.Top)
            return Direction.Bottom;
        else if (direction == Direction.Bottom)
            return Direction.Top;
        else if (direction == Direction.Right)
            return Direction.Left;
        else
            return Direction.Right;
    }

    public void TakeDamage()
    {
        health--;
        UIManager.Instance.SetHealth(health);
        if (health <= 0)
            GameOver();
    }

    private void GameOver()
    {
        isGameOver = true;
        PlaySound(SoundType.GameOver);
        DelayFunction(currentLevel.introSlide.Show, 4f);
        DelayFunction(currentLevel.ResetLevel, 3.5f);
    }

    public void DelayFunction(Action function, float delay, Action callback = null)
    {
        if (delay == 0)
        {
            function();
            if (callback != null)
                callback();
        }
        else
            StartCoroutine(DelayFunctionCoroutine(function, delay, callback));
    }

    private IEnumerator DelayFunctionCoroutine(Action function, float delay, Action callback)
    {
        yield return new WaitForSeconds(delay);
        function();
        if (callback != null)
            callback();
    }

    public void RemoveQuest(Quest quest)
    {
        currentLevel.RemoveQuest(quest);
    }
}
