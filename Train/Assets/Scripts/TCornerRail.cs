﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCornerRail : Rail
{
    public override Direction GetDirection(Direction fromDirection)
    {
        return GameManager.InvertedDirection(fromDirection) == Direction2 ? Direction1 : Direction2;
    }

    [SerializeField]
    private Direction tCornerDirection;
    [SerializeField]
    private Direction variableDirection;
    private bool isSwitched = false;

    public void Switch()
    {
        GameManager.Instance.PlaySound(SoundType.SelectRail);
        isSwitched = !isSwitched;
        //if (tCornerDirection == Direction.Bottom || tCornerDirection == Direction.Top)
        spriteRenderer.flipX = !spriteRenderer.flipX;
        //else
        //    spriteRenderer.flipY = !spriteRenderer.flipY;
    }

    private Direction Direction1
    {
        get
        {
            return GameManager.InvertedDirection(tCornerDirection);
        }
    }

    private Direction Direction2
    {
        get
        {
            return isSwitched ? GameManager.InvertedDirection(variableDirection) : variableDirection;
        }
    }

    protected void OnMouseDown()
    {
        Switch();
    }
}
