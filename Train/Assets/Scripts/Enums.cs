﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Top,
    Right,
    Bottom,
    Left
}

public enum TrainColor
{
    Red,
    Blue,
    Yellow,
    Green
}

public enum SoundType
{
    Crash,
    PickQuest,
    CompleteQuest,
    WrongQuest,
    GameOver,
    SelectRail
}

