﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntersectionRail : Rail
{
    public override Direction GetDirection(Direction fromDirection)
    {
        if (GameManager.InvertedDirection(fromDirection) == Direction2)
            return Direction1;
        if (GameManager.InvertedDirection(fromDirection) == Direction1)
            return Direction2;
        return fromDirection;
    }

    protected void OnMouseDown()
    {
        Switch();
    }

    private int direction;

    public void Switch()
    {
        GameManager.Instance.PlaySound(SoundType.SelectRail);
        direction = (direction + 1) % 4;
        transform.localEulerAngles = new Vector3(0, 0, -90 * direction);
    }

    private Direction Direction1
    {
        get
        {
            return (Direction)direction;
        }
    }

    private Direction Direction2
    {
        get
        {
            return (Direction)((direction+1)%4);
        }
    }
}
