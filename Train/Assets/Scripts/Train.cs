﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Train : Carriage
{
    private List<Carriage> carriages = new List<Carriage>();
    [SerializeField]
    private SpriteRenderer starRenderer;
    public void Init(Rail startingRail, Direction startingDirection, float speedModifier, TrainColor color, List<Carriage> carriages)
    {
        starRenderer.enabled = false;
        Init(startingRail, startingDirection, speedModifier, color);
        this.carriages = carriages;
        int index = 0;
        foreach (var carriage in carriages)
        {
            List<Rail> route = new List<Rail>();
            //route.Add(startRail);
            for (int i = 0; i < index; i++)
                route.Add(carriages[i].startRail);
            route.Reverse();
            carriage.AddTargetRails(route, false);
            index++;
        }
        foreach (var carriage in carriages)
            carriage.TryStartMove();
        MoveTo(startRail.transform.position + new Vector3(MovingDirection == Direction.Right ? 16 : (MovingDirection == Direction.Left ? -16 : 0), MovingDirection == Direction.Top ? 16 : (MovingDirection == Direction.Bottom ? -16 : 0), 0));
    }

    private Quest selectedQuest;

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(other.tag);
        if (other.tag == "rail")
        {
            Rail rail = other.GetComponent<Rail>();
            AddRailToCarriages(rail);
            nextMovingDirection = rail.GetDirection(MovingDirection);
            //Debug.Log(nextMovingDirection);
        }
        else if (other.tag == "carriage" || other.tag == "train")
        {
            if (GameManager.Instance.IsGameOver)
                Freeze();
            Carriage carriage = other.GetComponent<Carriage>();
            if (!IsFrozen && !carriage.IsFrozen && !carriages.Contains(carriage))
            {
                GameManager.Instance.PlaySound(SoundType.Crash);
                TakeDamage();
            }
        }
        else if (other.tag == "box")
        {
            QuestBox box = other.GetComponent<QuestBox>();
            if (box.quest.color == trainColor)
            {
                GameManager.Instance.PlaySound(SoundType.PickQuest);
                starRenderer.enabled = true;
                box.quest.destinationPoint.AddQuest(trainColor);
                selectedQuest = box.quest;
            }
            else
            {
                GameManager.Instance.PlaySound(SoundType.WrongQuest);
                GameManager.Instance.AddMoney(-1);
                GameManager.Instance.RemoveQuest(box.quest);
            }
            Destroy(other.gameObject);
        }
        else if (other.tag == "station")
        {
            Station station = other.GetComponent<Station>();
            if (selectedQuest != null && selectedQuest.destinationPoint == station)
            {
                GameManager.Instance.PlaySound(SoundType.CompleteQuest);
                starRenderer.enabled = false;
                station.RemoveQuest(selectedQuest.color);
                GameManager.Instance.AddMoney(2);
                GameManager.Instance.RemoveQuest(selectedQuest);
                selectedQuest = null;
            }
        }
    }

    private void TakeDamage()
    {
        GameManager.Instance.TakeDamage();
        Freeze();
        GameManager.Instance.DelayFunction(Unfreeze, 3.9f);
    }

    public override void Freeze()
    {
        base.Freeze();
        foreach (var carriage in carriages)
            carriage.Freeze();
    }

    public override void Unfreeze()
    {
        base.Unfreeze();
        foreach (var carriage in carriages)
            carriage.Unfreeze();
    }

    private Direction nextMovingDirection;

    private void AddRailToCarriages(Rail rail)
    {
        foreach (var carriage in carriages)
            carriage.AddTargetRail(rail);
    }

    protected override void OnFinishedMove()
    {
        MovingDirection = nextMovingDirection;
        MoveTo(transform.position + new Vector3(MovingDirection == Direction.Right ? 16 : (MovingDirection == Direction.Left ? -16 : 0), MovingDirection == Direction.Top ? 16 : (MovingDirection == Direction.Bottom ? -16 : 0), 0));
    }
}
