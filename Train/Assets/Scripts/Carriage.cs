﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carriage : MonoBehaviour
{
    [SerializeField]
    protected Sprite verticalTopSprite;
    [SerializeField]
    protected Sprite horizontalRightSprite;
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    public Rail startRail;

    private Direction movingDirection;
    protected Direction MovingDirection 
    {
        get
        {
            return movingDirection;
        }

        set
        {
            if (value == movingDirection)
                return;
            else
            {
                movingDirection = value;
                if (movingDirection == Direction.Top)
                {
                    spriteRenderer.flipY = false;
                    spriteRenderer.sprite = verticalTopSprite;
                }
                else if (movingDirection == Direction.Bottom)
                {
                    spriteRenderer.sprite = verticalTopSprite;
                    spriteRenderer.flipY = gameObject.tag == "train" ? true : false;
                }
                else if (movingDirection == Direction.Right)
                {
                    spriteRenderer.sprite = horizontalRightSprite;
                    spriteRenderer.flipY = false;
                    spriteRenderer.flipX = false;
                }
                else if (movingDirection == Direction.Left)
                {
                    spriteRenderer.sprite = horizontalRightSprite;
                    spriteRenderer.flipY = false;
                    spriteRenderer.flipX = true;
                }
            }
        }
    }

    protected List<Rail> targetRailsQueue = new List<Rail>();
    protected Rail currentTargetRail;
    protected bool isMoving = false;
    protected float localSpeedModifier;
    protected TrainColor trainColor;

    public void Init(Rail startingRail, Direction startingDirection, float speedModifier, TrainColor color)
    {
        localSpeedModifier = speedModifier;
        trainColor = color;
        MovingDirection = startingDirection;
        startRail = startingRail;
        transform.position = startRail.transform.position;
    }

    public void AddTargetRail(Rail rail, bool tryStartMove = true)
    {
        targetRailsQueue.Add(rail);
        if (tryStartMove)
            TryStartMove();
    }

    public void AddTargetRails(List<Rail> rails, bool tryStartMove = true)
    {
        targetRailsQueue.AddRange(rails);
        if (tryStartMove)
            TryStartMove();
    }

    public void TryStartMove()
    {
        if (!isMoving && !isFrozen)
        {
            if (targetRailsQueue.Count > 0)
            {
                MoveTo(targetRailsQueue[0]);
                targetRailsQueue.RemoveAt(0);
            }
        }
    }

    private bool isFrozen = false;

    public virtual void Freeze()
    {
        isFrozen = true;
        moveDescription.pause();
        ToggleBlink(true);
    }

    private bool isBlinking;
    public void ToggleBlink(bool active)
    {
        blinkTime = 0f;
        isBlinking = active;
        if (spriteRenderer != null)
            spriteRenderer.enabled = !active;
    }
       
    public virtual void Unfreeze()
    {
        moveDescription.resume();
        ToggleBlink(false);
        isFrozen = false;
    }

    protected void MoveTo(Rail rail)
    {
        isMoving = true;
        currentTargetRail = rail;
        MoveTo(rail.transform.position);
    }

    private LTDescr moveDescription;
    protected void MoveTo(Vector3 location)
    {
        if (location.x - transform.position.x == 0)
        {
            if ((location.y - transform.position.y) > 0)
                MovingDirection = Direction.Top;
            else
                MovingDirection = Direction.Bottom;
        }
        else
        {
            if ((location.x - transform.position.x) > 0)
                MovingDirection = Direction.Right;
            else
                MovingDirection = Direction.Left;
        }
        int id = LeanTween.move(gameObject, location, 1f / SpeedModifier).id;
        moveDescription = LeanTween.descr(id);
        if (moveDescription != null)
        {
            moveDescription.setOnComplete(OnFinishedMove);
        }
    }

    protected float SpeedModifier
    {
        get { return localSpeedModifier * GameManager.GlobalSpeedMultiplier; }
    }

    protected virtual void OnFinishedMove()
    {
        isMoving = false;
        TryStartMove();
    }

    public bool IsFrozen
    {
        get { return isFrozen; }
    }

    private float blinkTime;
    private void Update()
    {
        //    transform.position = new Vector3((int)transform.position.x, (int)transform.position.y, 0);
        if (isBlinking)
        {
            blinkTime += Time.deltaTime;
            spriteRenderer.enabled = blinkTime % 1 > 0.5f;
        }
    }
}
